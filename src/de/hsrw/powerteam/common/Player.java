package de.hsrw.powerteam.common;

import de.hsrw.powerteam.util.AsciiColors;
import de.hsrw.powerteam.util.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class Player implements Serializable {

    public String name;
    public transient Location location;
    public int locationId;      //used to store location in player file, this is updated only before saving
    public int lvl;
    public int currentExp;
    public int thresholdExp;
    public int money;
    public int fame;
    public int maxHp;
    public int hp;
    public int pow;
    public int dex;
    public int intel;
    public ArrayList<Item> inventory;
    public Armor armor;
    public Weapon weapon;

    public Player() {
        //xml serializer needs this
        super();
    }

    public Player(String name, Location location) {
        Random random = new Random();
        this.name = name;
        this.location = location;
        this.pow = random.nextInt(10) + 1;
        this.dex = random.nextInt(10) + 1;
        this.intel = random.nextInt(10) + 1;
        this.maxHp = 10;
        this.hp = maxHp;
        fame = 0;
        lvl = 1;
        currentExp = 0;
        thresholdExp = 100;
        money = 10;
        inventory = new ArrayList<>();
    }

    public Player(String name, Location location, int pow, int dex, int intel) {
        this.name = name;
        this.location = location;
        this.pow = pow;
        this.dex = dex;
        this.intel = intel;
        this.maxHp = 10;
        this.hp = maxHp;
        lvl = 1;
        currentExp = 0;
        thresholdExp = 100;
    }

    public void addExp(int newExp) {
        currentExp += newExp;
        if (currentExp >= thresholdExp) {
            currentExp -= thresholdExp;
            thresholdExp += lvl * 50;
            maxHp += 10;
            hp = maxHp;
            pow += 5;
            dex += 5;
            intel += 5;
            lvl++;
        }
    }

    public String deequipArmor() {
        if (armor != null) {
            String armorName = armor.name;
            maxHp -= armor.maxHp;
            hp -= armor.maxHp;
            pow -= armor.pow;
            dex -= armor.dex;
            intel -= armor.intel;
            inventory.add(armor);
            armor = null;
            return "Deequiped " + armorName;
        } else {
            return "No armor to deequip";
        }
    }

    public String deequipWeapon() {
        if (weapon != null) {
            String weaponName = weapon.name;
            pow -= weapon.pow;
            dex -= weapon.dex;
            intel -= weapon.intel;
            inventory.add(weapon);
            weapon = null;
            return "Deequiped " + weaponName;
        } else {
            return "No weapon to deequip";
        }
    }

    private boolean fightPretty(Random random, int maxHp, int dex, int pow, int intel) {
        int enemyHp = maxHp;
        while (enemyHp > 0) {
            // Player
            int damageToTake = 0;
            boolean dodge = random.nextInt(100) <= this.intel;
            if (dodge) {
                System.out.println(TextUtils.colorString(AsciiColors.ANSI_GREEN,
                        "You dodged the attack!"));
            } else {
                boolean isCritical = random.nextInt(100) <= dex;
                if (isCritical) {
                    damageToTake = pow * 2;
                    System.out.println(TextUtils.colorString(AsciiColors.ANSI_RED,
                            "Received critical damage: ") + damageToTake);
                } else {
                    damageToTake = pow;
                    System.out.println(TextUtils.colorString(AsciiColors.ANSI_PURPLE,
                            "Received damage: ") + damageToTake);
                }
            }
            this.hp -= damageToTake;
            if (this.hp <= 0) {
                return false;
            }

            // Enemy
            int damageToDeal = 0;
            boolean enemyDodge = random.nextInt(100) <= intel;
            if (enemyDodge) {
                System.out.println(TextUtils.colorString(AsciiColors.ANSI_BLUE,
                        "Enemy dodged the attack!"));
            } else {
                boolean isCritical = random.nextInt(100) <= this.dex;
                if (isCritical) {
                    damageToDeal = this.pow * 2;
                    System.out.println(TextUtils.colorString(AsciiColors.ANSI_RED,
                            "Inflicted critical damage: ") + damageToDeal);
                } else {
                    damageToDeal = this.pow;
                    System.out.println(TextUtils.colorString(AsciiColors.ANSI_YELLOW,
                            "Inflicted damage: ") + damageToDeal);
                }
            }
            enemyHp -= damageToDeal;
            try {
                long timeToSleep = 1000 - this.dex;
                if (timeToSleep < 0)
                    Thread.sleep(0);
                else
                    Thread.sleep(timeToSleep);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
        System.out.println(TextUtils.colorString(AsciiColors.ANSI_CYAN,
                "HP left: " + this.hp));
        return true;
    }

    private boolean fight(Random random, int maxHp, int dex, int pow, int intel) {
        int enemyHp = maxHp;
        while (enemyHp > 0) {
            // Player
            int damageToTake = 0;
            boolean dodge = random.nextInt(100) <= this.intel;
            if (!dodge) {
                damageToTake = random.nextInt(100) <= dex ? pow * 2 : pow;
            }
            this.hp -= damageToTake;
            if (this.hp <= 0) {
                return false;
            }

            // Enemy
            int damageToDeal = 0;
            boolean enemyDodge = random.nextInt(100) <= intel;
            if (!enemyDodge) {
                damageToDeal = random.nextInt(100) <= this.dex ? this.pow * 2 : this.pow;
            }
            enemyHp -= damageToDeal;
        }
        return true;
    }

    public boolean fight(Mob mob, Random random, boolean pretty) {
        if (pretty) {
            if (fightPretty(random, mob.maxHp, mob.dex, mob.pow, mob.intel)) {
                int levelBefore = this.lvl;
                this.addExp(mob.xpReward);
                this.money += mob.moneyReward;
                if (levelBefore < this.lvl) {
                    System.out.println(TextUtils.colorString(AsciiColors.ANSI_PURPLE,
                            "You have reached level: " + this.lvl));
                    System.out.println(TextUtils.colorString(AsciiColors.ANSI_BLUE,
                            "HP fully restored!"));
                }
                return true;
            }
        } else {
            if (fight(random, mob.maxHp, mob.dex, mob.pow, mob.intel)) {
                this.addExp(mob.xpReward);
                this.money += mob.moneyReward;
                return true;
            }
        }
        return false;
    }

    public boolean fight(Player otherPlayer, Random random, boolean pretty) {
        if (pretty)
            if (fightPretty(random, otherPlayer.maxHp, otherPlayer.dex, otherPlayer.pow, otherPlayer.intel)) {
                fame += otherPlayer.lvl;
                return true;
            } else {
                return false;
            }
        else if (fight(random, otherPlayer.maxHp, otherPlayer.dex, otherPlayer.pow, otherPlayer.intel)) {
            fame += otherPlayer.lvl;
            return true;
        } else {
            return false;
        }
    }
}
