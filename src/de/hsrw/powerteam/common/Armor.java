package de.hsrw.powerteam.common;

import java.io.Serializable;

public class Armor implements Item, Serializable {

    public int id;
    public String name;
    public String description;
    public int maxHp;
    public int pow;
    public int dex;
    public int intel;
    public int value;

    public Armor() {
        super();
    }

    public Armor(int id, String name, String description, int maxHp, int pow, int dex, int intel, int value) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.maxHp = maxHp;
        this.pow = pow;
        this.dex = dex;
        this.intel = intel;
        this.value = value;
    }

    public String use(Player player) {

        String message;

        if (player.armor == null) {
            message = "Equiped " + name;
        } else {
            message = player.deequipArmor() + ", equiped " + name;
        }

        player.armor = this;
        player.maxHp += maxHp;
        player.hp += maxHp;
        player.pow += pow;
        player.dex += dex;
        player.intel += intel;
        return message;

    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getValue() {
        return value;
    }


}
