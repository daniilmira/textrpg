package de.hsrw.powerteam.common;

public interface Item {

    String getName();

    String getDescription();

    int getValue();

    String use(Player player);

}
