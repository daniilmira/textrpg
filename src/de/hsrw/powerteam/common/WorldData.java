package de.hsrw.powerteam.common;

import java.io.Serializable;
import java.util.ArrayList;

public class WorldData implements Serializable {

    public ArrayList<Location> locationList = new ArrayList<>();

    public Location getLocationById(int id) {
        for (Location location : locationList) {
            if (location.id == id)
                return location;
        }

        return null;
    }

    public Location getLocationByName(String name) {
        for (Location location : locationList) {
            if (location.name.equals(name))
                return location;
        }

        return null;
    }
}
