package de.hsrw.powerteam.common;

import java.io.Serializable;

public class NPC implements Serializable {
    public int id;
    public String name;
    public String dialogue;

    public NPC() {
        //xml serializer needs this
        super();
    }

    public NPC(int id, String name, String dialogue) {
        this.id = id;
        this.name = name;
        this.dialogue = dialogue;
    }
}
