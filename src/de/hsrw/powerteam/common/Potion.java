package de.hsrw.powerteam.common;

import java.io.Serializable;

public class Potion implements Item, Serializable {

    public int id;
    public String name;
    public String description;
    public int health;
    public int value;

    public Potion() {
        super();
    }

    public Potion(int id, String name, String description, int health, int value) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.health = health;
        this.value = value;
    }

    public String use(Player player) {

        if (player.hp + health > player.maxHp) {
            player.hp = player.maxHp;
        } else {
            player.hp += health;
        }
        return "You restored " + health + " hp!";

    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getValue() {
        return value;
    }

}
