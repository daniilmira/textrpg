package de.hsrw.powerteam.common;

import java.io.Serializable;

public class Inn implements Serializable {
    public int id;
    public String name;

    public Inn() {
        //xml serializer needs this
        super();
    }

    public Inn(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void getHealed(Player player) {
        player.hp = player.maxHp;
    }
}
