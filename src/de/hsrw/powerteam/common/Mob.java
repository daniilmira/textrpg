package de.hsrw.powerteam.common;

import java.io.Serializable;

public class Mob implements Serializable {

    public int id;
    public String name;
    public int maxHp;
    public int pow;
    public int dex;
    public int intel;
    public int xpReward;
    public int moneyReward;

    public Mob() {
        //xml serializer needs this
        super();
    }

    public Mob(int id, String name, int maxHp, int pow, int dex, int intel, int xpReward, int moneyReward) {
        this.id = id;
        this.name = name;
        this.maxHp = maxHp;
        this.pow = pow;
        this.dex = dex;
        this.intel = intel;
        this.xpReward = xpReward;
        this.moneyReward = moneyReward;
    }

}
