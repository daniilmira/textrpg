package de.hsrw.powerteam.common;

import java.io.Serializable;
import java.util.ArrayList;

public class Shop implements Serializable {

    public int id;
    public String name;
    public ArrayList<Item> stock;

    public Shop() {
        super();
    }

    public Shop(int id, String name) {
        this.id = id;
        this.name = name;
        stock = new ArrayList<>();
    }

}
