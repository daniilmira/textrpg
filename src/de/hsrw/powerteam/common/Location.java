package de.hsrw.powerteam.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Location implements Serializable {

    public int id;
    public String name;
    public String art;
    public List<Mob> mobList = new ArrayList<>();
    public ArrayList<NPC> npcList = new ArrayList<>();
    public ArrayList<Inn> innList = new ArrayList<>();
    public ArrayList<Shop> shopList = new ArrayList<>();
	public ArrayList<Location> adjacentLocations = new ArrayList<>();

    public Location() {
        //xml serializer needs this
        super();
    }

    public Location(int id, String name, String art) {

        this.id = id;
        this.name = name;
        this.art = art;

    }
}
