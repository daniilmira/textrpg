package de.hsrw.powerteam.common;

import java.io.Serializable;

public class Weapon implements Item, Serializable {

    public int id;
    public String name;
    public String description;
    public int pow;
    public int dex;
    public int intel;
    public int value;

    public Weapon() {
        super();
    }

    public Weapon(int id, String name, String description, int pow, int dex, int intel, int value) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.pow = pow;
        this.dex = dex;
        this.intel = intel;
        this.value = value;
    }

    public String use(Player player) {

        String message;

        if (player.weapon == null) {
            message = "Equiped " + name;
        } else {
            message = player.deequipWeapon() + ", equiped " + name;
        }

        player.weapon = this;
        player.pow += pow;
        player.dex += dex;
        player.intel += intel;
        return message;

    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getValue() {
        return value;
    }

}
