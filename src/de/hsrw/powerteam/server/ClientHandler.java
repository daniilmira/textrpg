package de.hsrw.powerteam.server;

import de.hsrw.powerteam.Main;
import de.hsrw.powerteam.common.Item;
import de.hsrw.powerteam.common.Mob;
import de.hsrw.powerteam.common.Player;
import de.hsrw.powerteam.util.AsciiColors;
import de.hsrw.powerteam.util.TextUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class ClientHandler implements Runnable {
    private final Socket clientSocket;
    private ObjectOutputStream out;
    private BufferedReader in;

    private Player player;

    public ClientHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        String command;
        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new ObjectOutputStream(clientSocket.getOutputStream());
            System.out.println("Client connected");

            this.write(TextUtils.colorString(AsciiColors.ANSI_RED, "Enter your name please."));
            this.write("/respond");
            try {
                if (player == null) {
                    command = readInput();
                    //check if player has a save file
                    if (XmlStorage.checkPlayerExists(command)) {
                        player = XmlStorage.loadPlayer(command, Main.world);
                        this.write(TextUtils.colorString(AsciiColors.ANSI_GREEN, "Player loaded"));
                        this.write(player);
                        this.write(Main.world);
                    } else {
                        player = new Player(command, Main.world.getLocationById(0));
                        XmlStorage.savePlayer(player);
                        this.write(TextUtils.colorString(AsciiColors.ANSI_GREEN, "Name registered."));
                        this.write(player);
                        this.write(Main.world);
                    }
                    servePlayer(false);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void restAtInn() throws IOException {
        if (player.location.innList.isEmpty()) {
            player.hp = player.maxHp - 10;
            if (player.hp <= 0) {
                this.write("you died while sleeping on the ground");
                disconnect(true);
            } else {
                this.write("");
                this.write("you have rested on the ground and regret it");
                this.write("");
                servePlayer(true);
            }
        } else {
            player.hp = player.maxHp;
            this.write("");
            this.write("You have rested in Inn: " + player.location.innList.get(0).name);
            this.write("/inn");
            this.write("");
            servePlayer(true);
        }
    }

    private void travel() throws IOException {
        this.write("");
        this.write("Location list you can travel from here:");

        for (int i = 0; i < player.location.adjacentLocations.size(); i++) {
            this.writeFormattedDecision(i, player.location.adjacentLocations.get(i).name);
        }
        this.write("/respond");

        try {
            int variant;
            String answer = readInput();

            if (answer.equals("back")) {
                servePlayer(true);
                return;
            }
            try {
                variant = Integer.parseInt(answer);
            } catch (NumberFormatException e) {
                processPlayer("travel");
                return;
            }

            if (variant > player.location.adjacentLocations.size() - 1) {
                processPlayer("travel");
                return;
            }
            player.location = player.location.adjacentLocations.get(variant);

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.write("");
        servePlayer(false);
        return;
    }

    private void talkWithNpc() throws IOException {
        this.write("");
        this.write("NPCs list you can talk with:");

        for (int i = 0; i < player.location.npcList.size(); i++) {
            this.writeFormattedDecision(i, player.location.npcList.get(i).name);
        }
        this.write("/respond");

        try {
            int variant;
            String answer = readInput();

            if (answer.equals("back")) {
                servePlayer(true);
                return;
            }
            try {
                variant = Integer.parseInt(answer);
            } catch (NumberFormatException e) {
                processPlayer("npc");
                return;
            }

            if (variant > player.location.npcList.size() - 1) {
                processPlayer("npc");
                return;
            }

            this.write("");
            this.write(player.location.npcList.get(variant).dialogue);

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.write("");
        servePlayer(true);
        return;
    }

    private void printPlayerStatus() throws IOException {
        this.write("");
        writeFormattedStat("Character Name:", player.name);
        writeFormattedStat("Level:", player.lvl);
        writeFormattedStat("Experience points:", player.currentExp);
        writeFormattedStat("Fame:", player.fame);
        this.write("---------------------------------------------");
        writeFormattedStat("Current HP:", player.hp);
        writeFormattedStat("Max HP:", player.maxHp);
        writeFormattedStat("Power:", player.pow);
        writeFormattedStat("Dexterity:", player.dex);
        writeFormattedStat("Intelligence:", player.intel);
        this.write("");

        servePlayer(true);
        return;
    }

    private void checkInventory() throws IOException {
        this.write("");
        this.writeFormattedStat("Money:", player.money);

        if (player.armor == null) {
            this.writeFormattedStat("Armor:", "---");
        } else {
            this.writeFormattedStat("Armor:", player.armor.name);
        }

        if (player.weapon == null) {
            this.writeFormattedStat("Weapon:", "---");
        } else {
            this.writeFormattedStat("Weapon:", player.weapon.name);
        }

        this.write("---------------------------------------------");

        if (!player.inventory.isEmpty()) {
            this.write("Inventory contains following items:");
            this.write("");
            for (int i = 0; i < player.inventory.size(); i++) {
                this.writeFormattedItem(i, player.inventory.get(i).getName());
            }
        } else {
            this.write("Inventory is empty");
        }
        this.write("");

        this.write("/respond");

        try {
            int variant;
            String answer = readInput();

            if (answer.equals("back")) {
                this.write("");
                servePlayer(true);
                return;
            }
            try {
                variant = Integer.parseInt(answer);
            } catch (NumberFormatException e) {
                processPlayer("inventory");
                return;
            }

            if (variant > player.inventory.size() - 1) {
                processPlayer("inventory");
                return;
            }

            checkItem(variant);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkItem(int itemNumber) throws IOException {

        Item item = player.inventory.get(itemNumber);
        this.write("");
        this.write(item.getName());
        this.writeFormattedDecision("use", "the item.");
        this.writeFormattedDecision("check", "the description.");
        this.write("/respond");

        try {
            String answer = readInput();

            if (answer.equals("back")) {
                this.write("");
                processPlayer("inventory");
                return;
            } else if (answer.equals("use")) {
                this.write("");
                this.write(item.use(player));
                player.inventory.remove(item);
                if (player.hp <= 0) {
                    this.write("");
                    this.write("Your wounds have killed you!");
                    disconnect(true);
                } else {
                    processPlayer("inventory");
                    return;
                }
            } else if (answer.equals("check")) {
                this.write("");
                this.write(item.getDescription());
                checkItem(itemNumber);
            } else {
                checkItem(itemNumber);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void checkShops() throws IOException {
        this.write("");

        if (player.location.shopList.isEmpty()) {
            this.write("This area has no shops");
            this.write("");
            servePlayer(true);
            return;
        } else {
            this.write("This area has following shops:");

            for (int i = 0; i < player.location.shopList.size(); i++) {
                this.writeFormattedDecision(i, player.location.shopList.get(i).name);
            }
            this.write("/respond");
        }

        try {
            int variant;
            String answer = readInput();

            if (answer.equals("back")) {
                this.write("");
                servePlayer(true);
                return;
            }
            try {
                variant = Integer.parseInt(answer);
            } catch (NumberFormatException e) {
                processPlayer("shop");
                return;
            }

            if (variant > player.location.shopList.size() - 1) {
                processPlayer("shop");
                return;
            }

            chooseTradeOption(variant);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void chooseTradeOption(int shop) throws IOException {
        this.write("");
        this.write("Are you buying or selling?");
        this.writeFormattedDecision("buy", "items");
        this.writeFormattedDecision("sell", "items");

        this.write("/respond");

        try {

            String answer = readInput();

            if (answer.equals("back")) {
                servePlayer(true);
                return;
            } else if (answer.equals("buy")) {
                buyItem(shop);
            } else if (answer.equals("sell")) {
                sellItem();
            } else {
                chooseTradeOption(shop);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void buyItem(int shop) throws IOException {


        this.write("");
        this.write("Money: " + player.money);
        this.write("");
        this.write(AsciiColors.ANSI_GREEN + "BUY BUY BUY BUY BUY BUY BUY BUY BUY");
        this.write("");

        ArrayList<Item> stock = player.location.shopList.get(shop).stock;

        for (int i = 0; i < stock.size(); i++) {
            this.writeFormattedShopItem(i, stock.get(i).getName(), stock.get(i).getValue());
            this.write(stock.get(i).getDescription());
            this.write("-----------------------------------------------------------");
        }
        this.write("");

        this.write("/respond");

        try {
            int variant;
            String answer = readInput();

            if (answer.equals("back")) {
                this.write("");
                servePlayer(true);
                return;
            }
            try {
                variant = Integer.parseInt(answer);
            } catch (NumberFormatException e) {
                buyItem(shop);
                return;
            }

            if (variant > player.location.shopList.get(shop).stock.size() - 1) {
                buyItem(shop);
                return;
            }

            Item item = player.location.shopList.get(shop).stock.get(variant);

            if (player.money >= item.getValue()) {
                player.money -= item.getValue();
                player.inventory.add(item);
                this.write("");
                this.write("Pleasure doing business!");
                buyItem(shop);
            } else {
                this.write("");
                this.write("Not enough money!");
                buyItem(shop);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void sellItem() throws IOException {
        this.write("");
        this.write("Money: " + player.money);
        this.write("");

        this.write(AsciiColors.ANSI_RED + "SELL SELL SELL SELL SELL SELL SELL SELL");
        this.write("");

        if (!player.inventory.isEmpty()) {
            this.write("Inventory contains following items:");
            this.write("");
            for (int i = 0; i < player.inventory.size(); i++) {
                this.writeFormattedShopItem(i, player.inventory.get(i).getName(), player.inventory.get(i).getValue() / 2);
            }
        } else {
            this.write("Inventory is empty");
        }
        this.write("");

        this.write("/respond");

        try {
            int variant;
            String answer = readInput();

            if (answer.equals("back")) {
                this.write("");
                servePlayer(true);
                return;
            }
            try {
                variant = Integer.parseInt(answer);
            } catch (NumberFormatException e) {
                sellItem();
                return;
            }

            if (variant > player.inventory.size() - 1) {
                sellItem();
                return;
            }

            player.money += player.inventory.get(variant).getValue() / 2;
            player.inventory.remove(variant);
            sellItem();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pve() throws IOException {
        Random random = new Random();
        this.write(random);
        this.write("");
        this.write("/pve");

        String input = readInput();
        if (input.equals("/ready")) {
            input = readInput();
        }

        Mob chosenMob;
        int variant;

        if (input.equals("back")) {
            servePlayer(true);
            return;
        }
        try {
            variant = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            processPlayer("pve");
            return;
        }

        if (variant > player.location.mobList.size() - 1) {
            processPlayer("pve");
            return;
        }

        chosenMob = player.location.mobList.get(variant);
        if (player.fight(chosenMob, random, false)) {
            servePlayer(true);
            return;
        } else {
            disconnect(true);
        }
    }

    private void pvp() throws IOException {
        ArrayList<Player> playersPvpList = new ArrayList<>();
        for (ClientHandler clientHandler : Main.clientHandlers) {
            if (clientHandler.player != null && !clientHandler.player.name.equals(player.name))
                playersPvpList.add(clientHandler.player);
        }

        if (playersPvpList.size() <= 0) {
            this.write("\nThere are no players in this location.\n");
            servePlayer(true);
            return;
        }
        Random random = new Random();
        this.write(random);
        this.write(playersPvpList);
        this.write("/pvp");

        String input = readInput();
        if (input.equals("/ready")) {
            input = readInput();
        }

        Player chosenPlayer;
        try {
            int variant;
            if (input.equals("back")) {
                servePlayer(true);
                return;
            }
            try {
                variant = Integer.parseInt(input);
                chosenPlayer = playersPvpList.get(variant);
            } catch (IndexOutOfBoundsException e) {
                processPlayer("pvp");
                return;
            }
        } catch (NumberFormatException e) {
            processPlayer("pvp");
            return;
        }

        if (player.fight(chosenPlayer, random, false)) {
            servePlayer(true);
        } else {
            disconnect(true);
            return;
        }

    }

    private void disconnect(boolean isDead) throws IOException {
        if (isDead) {
            XmlStorage.deletePlayer(player);
            this.write("/disconnect/true");
        } else {
            XmlStorage.savePlayer(player);
            this.write("/disconnect/false");
        }

        Main.clientHandlers.remove(this);
        try {
            this.clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processPlayer(String command) throws IOException {
        switch (command.trim()) {
            case "fight":
                pve();
                break;
            case "travel":
                travel();
                break;
            case "inn":
                restAtInn();
                break;
            case "npc":
                talkWithNpc();
                break;
            case "shop":
                checkShops();
                break;
            case "inventory":
                checkInventory();
                break;
            case "stats":
                printPlayerStatus();
                break;
            case "pvp":
                pvp();
                break;
            case "exit":
                disconnect(false);
            default:
                this.write("");
                servePlayer(true);
                break;
        }
    }

    private void servePlayer(boolean didReturned) throws IOException {
        if (!clientSocket.isClosed()) {
            if (!didReturned) {
                this.write(TextUtils.colorString(AsciiColors.ANSI_YELLOW,
                        "You have entered " + player.location.name));
                this.write("/travel/" + player.location.id);

            }
            this.write("/serve");
            this.write("/respond");

            if (!clientSocket.isClosed()) {
                String command = readInput();
                processPlayer(command);
            }
        }
    }

    /*private void writeFormattedInventory(String section, String name) {
        write(TextUtils.formattedItem(i, name));
    }*/

    private void writeFormattedItem(int i, String name) throws IOException {
        write(TextUtils.formattedItem(i, name));
    }

    private void writeFormattedShopItem(int i, String name, int cost) throws IOException {
        write(TextUtils.formattedShopItem(i, name, cost));
    }

    private void writeFormattedDecision(int i, String description) throws IOException {
        write(TextUtils.formattedDecision(i, description));
    }

    private void writeFormattedDecision(String command, String description) throws IOException {
        write(TextUtils.formattedDecision(command, description));
    }

    private void writeFormattedStat(String stat, int value) throws IOException {
        write(TextUtils.formattedStat(stat, value));
    }

    private void writeFormattedStat(String stat, String value) throws IOException {
        write(TextUtils.formattedStat(stat, value));
    }

    private void write(Object object) throws IOException {
        if (!clientSocket.isClosed()) {
            out.writeObject(object);
            out.flush();
        }
    }

    private String readInput() throws IOException {
        String input = "";
        if (in.ready()) {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        }

        if (!clientSocket.isClosed()) {
            input = in.readLine();
            System.out.println(input);
        }

        return input;
    }
}
