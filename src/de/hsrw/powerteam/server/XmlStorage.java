package de.hsrw.powerteam.server;

import de.hsrw.powerteam.common.Location;
import de.hsrw.powerteam.common.Player;
import de.hsrw.powerteam.common.WorldData;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.ArrayList;

public class XmlStorage {
    public static final String StorageFolder = "C:\\zGameStorage\\";
    public static final String StorageFolderPlayers = StorageFolder + "players\\";
    public static final String StorageFileWorld = StorageFolder + "world.xml";

    public static boolean checkPlayerExists(String playerName) {
        File file = new File(playerFile(playerName));
        return file.isFile();
    }

    public static void deletePlayer(Player player) {
        File file = new File(playerFile(player.name));
        file.delete();
    }

    public static Player loadPlayer(String playerName, WorldData world) {
        Player player = (Player) loadObject(playerFile(playerName));
        //set location by stored id
        player.location = world.getLocationById(player.locationId);
        return player;
    }

    public static void savePlayer(Player player) {
        File dir = new File(StorageFolderPlayers);
        if (!dir.exists())
            dir.mkdirs();
        //set id so it can be stored without saving the entire location object
        player.locationId = player.location.id;
        saveObject(playerFile(player.name), player);
    }

    public static boolean checkWorldExists() {
        File file = new File(StorageFileWorld);
        return file.isFile();
    }

    @SuppressWarnings("unchecked")
    public static void loadWorld(WorldData world) {
        world.locationList = (ArrayList<Location>) loadObject(StorageFileWorld);
    }

    public static void saveWorld(WorldData world) {
        File dir = new File(StorageFolder);
        if (!dir.exists())
            dir.mkdirs();
        saveObject(StorageFileWorld, world.locationList);
    }

    /**
     * generates the path to the player file by player name
     *
     * @param playerName players name
     * @return path for player file
     */
    private static String playerFile(String playerName) {
        return StorageFolderPlayers + playerName + ".xml";
    }

    private static void saveObject(String file, Object obj) {
        try (XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(file)))) {
            encoder.writeObject(obj);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static Object loadObject(String file) {
        Object obj = null;
        try (XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(file)))) {
            obj = decoder.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return obj;
    }
}

