package de.hsrw.powerteam.server;

import de.hsrw.powerteam.common.*;
import de.hsrw.powerteam.util.AsciiArts;


public class WorldCreator {


    public static WorldData createWorld() {
        WorldData world = new WorldData();

        Location field = new Location(0, "Field", AsciiArts.field);
        Location farm = new Location(1, "Farm", AsciiArts.farm);
        Location city = new Location(2, "City", AsciiArts.city);
        Location forest = new Location(3, "Forest", AsciiArts.forest);

        field.adjacentLocations.add(farm);
        farm.adjacentLocations.add(field);
        farm.adjacentLocations.add(city);
        city.adjacentLocations.add(farm);
        city.adjacentLocations.add(forest);
        forest.adjacentLocations.add(city);

        field.npcList.add(new NPC(0, "Farmer",
                "Im tired of these motherfucking beetles on my motherquacking fields."));
        farm.npcList.add(new NPC(1, "Elder farmer",
                "Stay awhile and listen!"));
        city.npcList.add(new NPC(2, "Citizen",
                "Orange man bad!"));
        city.npcList.add(new NPC(3, "Guard",
                "Used to be an adventurer like you."));
        forest.npcList.add(new NPC(4, "Hunter",
                "Do not fight the troll. Very bad idea."));

        farm.innList.add(new Inn(0, "Barn"));
        city.innList.add(new Inn(1, "Quacking duck"));

        Shop apothecary1 = new Shop(0, "Apothecary");
        apothecary1.stock.add(new Potion(0, "Small health potion",
                "Small flask, heals 3 hp on use", 3, 20));
        apothecary1.stock.add(new Potion(1, "Medium health potion",
                "Medium flask, heals 5 hp on use", 5, 30));
        apothecary1.stock.add(new Potion(2, "Large health potion",
                "Large flask, heals 10 hp on use", 10, 50));

        Shop armory1 = new Shop(1, "Armory");
        armory1.stock.add(new Armor(0, "Full plate",
                "Sturdy and menacing (+5 max hp, +5 power)", 5, 5, 0, 0, 100));
        armory1.stock.add(new Armor(1, "Leather armor",
                "Light and easy to move in (+3 max hp, +7 dexterity)", 3, 0, 7, 0, 100));
        armory1.stock.add(new Armor(2, "Arcane robe",
                "Helps you meditate (+10 intelligence)", 0, 0, 0, 10, 100));

        Shop wpnsmith1 = new Shop(2, "Weaponsmith");
        wpnsmith1.stock.add(new Weapon(0, "Sword",
                "Perfect for slaying some goblins (+10 power)", 10, 0, 0, 100));
        wpnsmith1.stock.add(new Weapon(1, "Bow",
                "Snipers best friend (+10 dexterity)", 0, 10, 0, 100));
        wpnsmith1.stock.add(new Weapon(2, "Staff",
                "When in doubt, shove into enemy face (+10 intelligence)", 0, 0, 10, 100));

        city.shopList.add(apothecary1);
        city.shopList.add(armory1);
        city.shopList.add(wpnsmith1);

        field.mobList.add(new Mob(0, "Beetle", 10, 1, 1, 1, 10, 10));
        field.mobList.add(new Mob(1, "Worm", 8, 1, 3, 2, 20, 15));
        forest.mobList.add(new Mob(2, "Wolf", 15, 1, 5, 2, 30, 40));
        forest.mobList.add(new Mob(3, "Goblin", 10, 3, 6, 5, 40, 50));
        forest.mobList.add(new Mob(4, "Troll", 50, 20, 10, 1, 9999, 1000));
        field.mobList.add(new Mob(5, "Money chest", 10, 0, 0, 0, 10, 1000));
        field.mobList.add(new Mob(6, "Spooky scarecrow", 10, 5, 5, 5, 1, 1));

        world.locationList.add(field);
        world.locationList.add(farm);
        world.locationList.add(city);
        world.locationList.add(forest);

        return world;
    }
}
