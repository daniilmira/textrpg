package de.hsrw.powerteam.client;

import de.hsrw.powerteam.common.Mob;
import de.hsrw.powerteam.common.Player;
import de.hsrw.powerteam.common.WorldData;
import de.hsrw.powerteam.util.AsciiColors;
import de.hsrw.powerteam.util.TextUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.StringTokenizer;

public class GameClient {
    private Socket socket;
    private PrintWriter out;
    private ObjectInputStream in;
    private Scanner scanner;

    private WorldData world;
    private Player localPlayer;
    private ArrayList<Player> playersPvpList;
    private Random random;

    public GameClient() {
        init();
        loop();
    }

    private void init() {
        try {
            socket = new Socket("localhost", 8888);
            System.out.println("Connection established");

            out = new PrintWriter(socket.getOutputStream(), true);
            in = new ObjectInputStream((socket.getInputStream()));
            scanner = new Scanner(System.in);
        } catch (IOException e) {
            System.out.println("Server offline.");
            System.exit(1);
        }
    }

    private void loop() {
        while (!socket.isClosed()) {
            try {
                Object objIn = in.readObject();
                if (objIn instanceof String) {
                    String messageFromServer = (String) objIn;
                    serverMessageHandler(messageFromServer);
                } else if (objIn instanceof WorldData) {
                    world = (WorldData) objIn;
                } else if (objIn instanceof Player) {
                    localPlayer = (Player) objIn;
                } else if (objIn instanceof Random) {
                    random = (Random) objIn;
                } else if (objIn instanceof ArrayList<?>) {
                    if (((ArrayList<?>) objIn).get(0) instanceof Player)
                        playersPvpList = (ArrayList<Player>) objIn;
                }
            } catch (Exception e) {
                System.out.println("Socket exception. TCP problem.");
                break;
            }
        }
    }

    private void serverMessageHandler(String messageFromServer) {
        if (!messageFromServer.startsWith("/")) {
            System.out.println(messageFromServer);
            return;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(messageFromServer, "/");
        String cmd = stringTokenizer.nextToken();

        switch (cmd) {
            case "respond":
                askForInput();
                break;
            case "disconnect":
                disconnect(Boolean.parseBoolean(stringTokenizer.nextToken()));
                break;
            case "travel":
                localPlayer.location = world.getLocationById(Integer.parseInt(stringTokenizer.nextToken()));
                printCurrentLocation();
                break;
            case "serve":
                printCurrentLocationActions();
                break;
            case "pvp":
                pvp();
                break;
            case "pve":
                pve();
                break;
            case "inn":
                this.localPlayer.hp = this.localPlayer.maxHp;
                break;
            default:
                System.out.println(cmd + ": unhandled command");
                break;
        }
    }

    private void pvp() {
        System.out.println("Players in this location:");
        for (int i = 0; i < playersPvpList.size(); i++) {
            if (!playersPvpList.get(i).name.equals(localPlayer.name)) {
                printFormattedDecision(i, playersPvpList.get(i).name);
            }
        }
        System.out.print("Type here: ");
        String input = scanner.nextLine();

        Player chosenPlayer;
        try {
            int variant;
            if (input.equals("back")) {
                this.write(input);
                return;
            }
            try {
                variant = Integer.parseInt(input);
                chosenPlayer = playersPvpList.get(variant);
            } catch (IndexOutOfBoundsException e) {
                pvp();
                return;
            }
        } catch (NumberFormatException e) {
            pvp();
            return;
        }

        write("/ready");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        write(input);

        if (localPlayer.fight(chosenPlayer, random, true)) {
            System.out.println();
            System.out.println(TextUtils.colorString(AsciiColors.ANSI_GREEN,
                    "You won a fight with: " + chosenPlayer.name));
        } else {
            System.out.println(TextUtils.colorString(AsciiColors.ANSI_RED,
                    chosenPlayer.name + " have killed you."));
            return;
        }
        System.out.println();
    }

    private void pve() {
        System.out.println("Mobs you can fight with:");
        for (int i = 0; i < localPlayer.location.mobList.size(); i++) {
            printFormattedDecision(i, localPlayer.location.mobList.get(i).name);
        }

        System.out.print("Type here: ");

        String input = scanner.nextLine();

        System.out.println();

        Mob chosenMob;
        int variant;
        if (input.equals("back")) {
            this.write(input);
            return;
        }

        try {
            variant = Integer.parseInt(input);
            if (variant > localPlayer.location.mobList.size() - 1) {
                pve();
                return;
            } else {
                write("/ready");
            }
        } catch (NumberFormatException e) {
            pve();
            return;
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        write(input);

        chosenMob = localPlayer.location.mobList.get(variant);
        out.flush();
        if (localPlayer.fight(chosenMob, random, true)) {
            System.out.println();
            System.out.println(TextUtils.colorString(AsciiColors.ANSI_GREEN,
                    "You have killed: " + chosenMob.name));
        } else {
            System.out.println(chosenMob.name + " have killed you.");
            return;
        }
        System.out.println();
    }

    private void printCurrentLocation() {
        System.out.print(localPlayer.location.art);
    }

    private void printCurrentLocationActions() {

        if (!localPlayer.location.mobList.isEmpty()) {
            printFormattedDecision("fight", "Fight mobs in this location.");
        }
        if (!localPlayer.location.adjacentLocations.isEmpty()) {
            printFormattedDecision("travel", "Travel to the other location.");
        }
        if (!localPlayer.location.npcList.isEmpty()) {
            printFormattedDecision("npc", "Talk to the NPCs in this location.");
        }
        if (!localPlayer.location.innList.isEmpty()) {
            printFormattedDecision("inn", "Sleep to restore your health.");
        }
        if (!localPlayer.location.shopList.isEmpty()) {
            printFormattedDecision("shop", "Check local shops.");
        }
        
        printFormattedDecision("inventory", "Check your inventory.");
        printFormattedDecision("pvp", "Fight another player in this location.");
        printFormattedDecision("stats", "Get your character status.");
    }

    private void printFormattedDecision(String command, String description) {
        System.out.println(TextUtils.formattedDecision(command, description));
    }

    private void printFormattedDecision(int i, String description) {
        System.out.println(TextUtils.formattedDecision(i, description));
    }

    private String askForInput() {
        System.out.print("Type here: ");
        String s = scanner.nextLine();
        this.write(s);

        return s;
    }

    private void disconnect(boolean isDead) {
        if (isDead)
            System.out.println(TextUtils.colorString(AsciiColors.ANSI_RED,
                    "You are dead! - purging save file..."));

        System.out.println("You are disconnected.");
        try {
            this.socket.close();
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void write(String s) {
        out.println(s);
        out.flush();
    }
}
