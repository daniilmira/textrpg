package de.hsrw.powerteam.util;

public class AsciiArts {
    public static final String sunset =
            " (              ````                                            \n" +
                    ";`             ;;                                               \n" +
                    " ;;  -\"\"-.   ;;                 -;'  -.                         \n" +
                    "   \"\"     ``                      `.   `.                       \n" +
                    "                                    ;    `                      \n" +
                    "           `;                  -          ;         -.        ;`\n" +
                    "             `-  `.         .'`  .-'             .--`  ;     ;  \n" +
                    "              ;    `-.   ;    `-'             .;`     ;       `.\n" +
                    "              .        ``                                       \n" +
                    "               `            .--------.             .'           \n" +
                    "             ...        .--'``````````'--.        ;.            \n" +
                    "            `      . .-' .``          ``. '-. .      `.         \n" +
                    "          ;-.;  .  .' .`                  `. '.  .    ;         \n" +
                    "              .' .' .`                      `. '. '.    .       \n" +
                    "_____/'.-..___________________________ mvn,, ___________________\n" +
                    "                             )\\     nMmIEFooPTn                 \n" +
                    "                            ( (    Li1iiJl1ItTIjp               \n" +
                    "                             ) \\  i i_BP_LWmKK`  J  `           \n" +
                    "`                .          /  (   i1 LL 1I`L            .      \n" +
                    "             ..             \\   \\  i   X  Y o1                  \n" +
                    "                        .    )   )  `   l   p      ..           \n" +
                    ";                           /   (,      l(@) l                ..\n" +
                    "  q      ` .  '            (     \\.     i    p   R          .;  \n" +
                    "   \\  t            ;        )     \\`   j,.. ,.q,/Pqoj          `\n" +
                    "    \\/            `       ./       \\`;     `'     `          .. \n" +
                    "  '-     \\;            -'.'    ;    \\ `                     `. `\n" +
                    ".--.`.; ,-.. ,.-, ;' `.-'       `    `.'.   .--.\"\"-._        .; \n" +
                    "    `............---\"\"     ;_.         )   (  '=    /         `-\n" +
                    " ~                                    /     `------'     .      \n" +
                    "                 ~                  ,'  \\|//            `'      \n" +
                    "                ~           ~       ; `. \"\"                   ..\n" +
                    "                                     `.  )     \\\"       .--\"\"\"\n";

    public static final String field =
            "  `    `soso+m+     --+syhysm+    ::-hyhhyyo:    `s/.+yssdhh/s`     .`   `ohoos/-       `.--:+--+d:.\n" +
                    " --   :.mh/y+Nm /   `-+yhodsyM+    s.+yo/:mddo`     -:ody:/osdh/:   `/y` so+s+s+hh-``   ``         `\n" +
                    " y`  `y`momhosM/-`   `-dd+o+hyN-     .hyds-//mN/     :./mohsy+shm+o.  -o..yhhhyhy/h+mo/` :-:        \n" +
                    ":s   ..sNmyhdyMd`    -.ohNyd+dom+    /-:hdhso+:Md/    -.:sysh::s-.mm-`  ``:.ohho-+yoh+sm/  ---.     \n" +
                    ".`   -`dyomodyNN /   `+ homddmyNN`   `o--hyosdohNN:.      om:s+oyhmyyNo-  .:..ymhdh:o:/oNds/s:-/+`  \n" +
                    "     ``mN+:h/:Nd.`    - yMy./osyMh`    ` yydNsh:h/+M-    .`odsMN+//-:dNh     `.-.mhhhN+/++.+mN/` -/-\n" +
                    "    `+ Nsmsmd+oms     o//ddhdmhsMM+    /d-.m:yssmNhym/    `--/dhsmsdNN+Nd+d+   ss +dN/o//++NMhsNssh-\n" +
                    "    `o`hoyho`hsMh`/   .d hN/:s.-hNM:    .s`mNym:`s:.hNd.    y:/ydNmhos/-o:dN:  `-+s`ydy+MMmy/d.-y+Nh\n" +
                    "    -:NomyosdmhhM-`   `+`NshNomNd+Mm.   `y/`yM/m/ymoshM/`   `o+`.+s::/::dmdmdN+`  //:`omoN/+`/: +doM\n" +
                    "    `:Nhsyy+:NNhNs      -hydddy/mdsmm`    // +Momddh:mhomm.    .:/mm/ydd-.s+s:Mm/   `o- +yMds:dsosyh\n" +
                    "     /My:od-.ydMM-     .`/dds.msodNhNy     o: +Nhy/h.d+/dMh     /..yoNdhs:s:mMNNMo-/- :/-.-yodMy+: `\n" +
                    "     dhyMMohMNoNMs +`   +:dhhhhMN/sshMh     d..m-o+shhmh+oNy`    oy. :mddyydsm+:ho:hM+  /y.  .No+yom\n" +
                    "    .NMhsys::+/sNh h`   ./MMs-dsy+/NhNMy    .+-dMohy. oo.oNMMs    `+` oymMhhy/o hs+ymh/` .o+` shM/mM\n" +
                    "    +m+NMdsmyhmddm-/`    -M/+sNmNMmssMMN:    -o sMosdoMMNohMMNhh/  .o+  +N-::`-dmdddsdmh+: -+/  /dhM\n" +
                    "    :MNooyyss+.:Nh`:`    .Nhym-  y/mhmMMd`    .s`/NhMd//d+:ho-:MN`  `:+ -mMyyMM: `.`.mNdNd.   o+  :N\n" +
                    "   -Nmyd/oy/ `/ohNd`s   `.sMmsN/so.N/:+yM+     m:`hMssN/o- ysMdsNm     + `hMMNo/sy:y.d/+hNd   `ds``y\n" +
                    "   `M: ++/h+yhyy:ms      s.hm+Ny:oNN.hMNMs     /d `N+-+ `-`N`dhoN/     +h- -dN/mmNsy+mdMMMMs   `+No \n" +
                    "   .NMmooddy`.mMMN.+     :y NM:sNNdM/NMMh       -`-mMm.-sMdM-dMMN-      `s--mdyyhNNsoh.+Msmo      o/\n" +
                    "  `o.yMMM:dMm/sMMN:/      m`./dMMdyMMMs-``        ``oMMM+yMMs:mmy.       /o.  +MhoMMMMyso`          \n" +
                    "  -h  so/dMdm-dM/ .d      h-   ss mMs/  -d`       yo /::yNNhydM-o          +y` ` /ddMM: .y:         \n" +
                    "  :+      mym -:  -s      /+     /mM`    ++       `y:     dm+`` o/          :s`  .h.+M`  :d/        \n" +
                    "  oo      dym:    `h-     .d`    h/M/    .h        -h-    dM+   `os          :s`  +yoN    `o:       \n" +
                    "  o:      my:      h-      y-    `.Mo     +:        :h`   .Mo     s:          -h  -.oM`     -o      \n" +
                    "          -.                       /.                .`    -`                       `-              \n" +
                    "                                                                                                    \n";

    public static final String farm = "                                                                                                    \n" +
            "                        `/o.                                                                        \n" +
            "                      :++.`dh:                                                                      \n" +
            "                   -o+.     +Nm+`                                                                   \n" +
            "                 :s/         `oNNs.                                                                 \n" +
            "               /s/            -osdN.                               `                                \n" +
            "              .yy-.::::::://:/+yMMMm+`                            /+o`                              \n" +
            "            :s+`                -hMMMMy-                        `+sh++                              \n" +
            "         -sh/````                 -mMMMN:                       `y//d                               \n" +
            "        /ms:-----------.....`     +MMMMMo.                      -y  m                               \n" +
            "      :s+`                         -dMMMMMmo-                   /msom-                              \n" +
            "   .sd+...``````                     +NMMMMMMms-               d+ss :y                              \n" +
            "   +odo+++++oooooooooooooooooooooossysMMMMMMMMMMm-            -s oo :y                              \n" +
            "     d`                           /:d-M+MddsNhMN.             s- s/ :y                              \n" +
            "     m   :`  `                 `  :+m.M+Myy/mMMN              h  h/ :y    `.-:///:/s.               \n" +
            "    .d   s` .+   :`   -/       h  -om.MhMyo+dMMM.        ```./y::ms+yhssoo/:.`.oshs`s.              \n" +
            "    :y   s  .+   s.   -+   `-  y  .od.MMMyohyyys+//++/++///::--.``      /    /h. sMd`s:             \n" +
            "    oo   :  .+   y`   .o   `: `s  `oh-NMNm+`        ``        .` -`    /:   ss    sMm.s/            \n" +
            "    y/      -/   h`   `o    `  +   yN.hNm:   : :.  `/   -.   -. .-    -+  .do      sMm.o/           \n" +
            "    d-      o`   h`   `/       y   dN.dNd/./y:h+-/+o::/yo-:/y/+s+/:-:h+.  /h. .-:-` sMm.++          \n" +
            "  -:ho+++oo++++++h+++++++++++++ho++hN-hm:  .`o:  .y` `y:  `s. :+    s+   so  sMN-yh  sMm.+o         \n" +
            "  d---....`````                     M/d:   `s.  -y` `y.  `s. :s    s/  .h:   hMN sh   sMN./s        \n" +
            "  -:://dMMMMMMMMMMMNNNNNNmmmmmmdddhhMNy//+yh+++yh++shsoooyo+sh++//y-  :d.    hMM sy    sMN-:s       \n" +
            "       /M.`````````````````````````-y+   //   -o  -y`   .  /+   `s.   +y     -// ..     yMN-:y      \n" +
            "       .M`        `.`            `oo`    .    +  :y       /+   .s`  .y/            ``...-dMy:sh`    \n" +
            "       `M`       y/:+m:         :d:........`````/h--:----+s.``-s   `ms         .omMMMMMMMMMM+ .`    \n" +
            "       `N`       h  yMm         `-oMMMMMMMMMMMMMNoh+ ``/:////yy/:. +h.       .oNMMMMMMMMMMN/so      \n" +
            "       .N        y  hMM`         :NMmmmmMNmmmNNy`+s`  //    `+   .ho        +mMMMMMMMMMMMm-  os`    \n" +
            "       :d       :+  hMN`          yM.   +/     `s:   o:         om:       -hMMMMMMMMMMMMd`    /h-   \n" +
            "       +s      s///:///:          yM.   +:    -dsooohhoooo++++/dm`-/    -hMMMMMMMMMMMMMy`osss  om-  \n" +
            "       s/      +++++++o/          yM-   +:      d.o    /+++``.h+h ym    ```s-h``...-m:m        ss   \n" +
            "       h.                         yM-   +-     `h.+    ////   y-d yN       s.h      d m        ss   \n" +
            "       m                          hM-   +-     .s-+    +///   y-d sd       y`h      m m        ss   \n" +
            "      .h++++++///:--..```         /N/.` :`     -yoy++//yysy/:.y-m          h`d      d m        ss   \n" +
            "               ``.--::///+++++/+++:..-::///////yoos////yyyh++/d-N          h d+////`d m        so   \n" +
            "                                               m`::    ////``.h-N          h m++//: d m        o+   \n" +
            "                                               :/+++++oyhdo:-.-.N``.--:///+m m      h s        s+   \n" +
            "                                                          `.-:/++/::-.``  .h+hooo+oooo++ooo++oo:    \n";

    public static final String city = ".`..`:`./`   --.  `.-.:----.` `-../:-   -..`-`    :-                              /d`               \n" +
            ":`.``..:o.s:.-`-`.-:..-```/--/:-.-.   ``o-:. .`   :`-`                            :m`               \n" +
            ":  .`/`-+`:`-.`/...--..--.-.-`:..-+..--``.`     -``-:                             :m`               \n" +
            ":./---`o.:.`/::/.   -+:` .+-`   `-y.:--`        :-`:.    ...                      +N-               \n" +
            "-`./. +m/-`.:```           `///.:-d``          -.-.`-..``.:.                      +m+               \n" +
            ".:/` .o/d.                `-.    -Nh     ```:-.:.--.-.-``:`                      `oyN.              \n" +
            "`-   y:+sh                       `ms    `:. .`      .:-`.                        /ssNm`             \n" +
            "    -s.sdso                      +yd`            .-:--.`                        `y:symy             \n" +
            "   `h.-syhh/                    .h:yy`.---.`.-:-./-                        //   :--yohN+.:+s:       \n" +
            ":::s. /+oyyds+++-               s//+sy` ````                            `   yo+/o. :..+s++oo/    `  \n" +
            "y /-`./ooyyoyo`-+              ++--::/y.                      ``` ..`.`     s-.o```-..-+h+oy+`:--o.-\n" +
            "+:: `./hyhhdddh++            .o:...o/++s+        -:--.-.-://..`::--        `hdy-  -  `.-+sNd`       \n" +
            "mo-/+/+sohsyyhodd+          +h.  `---/+shh:                                 -ho///+++/++++oy/       \n" +
            "o-    . ``...:/+dm`       -o.``    ..//:+/ho:                               .+h:--...-:+oom/        \n" +
            "s++/+++osyyhyhhyyd      .o:-/-::::::oysyoso/sdo.                           ::.o::://:/:/oyoo++`     \n" +
            "yhso+::-/-://oohm-   `/+o:-`      `  ..``:///syhmy/                       -/ ``...:--:-:+ooood.     \n" +
            "yy.``   :/-+yyydm.   ++-://ssooshshsyhhhyhsohhhhmdh                     -yhdyyyho/:::/+o+oyoodmd:   \n" +
            "do/`.  .-:.:+yshd.   /mNymdyym:oy:o//oyoshshN+hmhh/                     y:-``      ` .. ..-::/syN-  \n" +
            "so/` `-:::.-+yosh-    `:+ss-`````..-:--o+sshN/smh                      `yo+y+:hs:ds:dm/sN+/yo+oyN-  \n" +
            ":o-``. :`::/dmhyy.       +/...-   `./.-+/sodm/ys   ``--                  :ohs/so+oo+so+syoyd+mm++`  \n" +
            "-/:`.. :-.-o+dhdNyyys+:.`+.:`.   `oohy/+oyshooddosyysdhs-        .y       s:/+:`-`//`  ``:-o+ymo    \n" +
            "./:`    ``.++`yNNdhmhmydhd:lll:``mNNNMsoydooohsdll-:mooohhsoooyysmd+      +hy/--`. `...++:--/s+y    \n" +
            "yhmsyss+:+-oy  +mhyhmyyysm--.::``shsysoooh/soshy:`+dososy////:- /s/s:     +hyo:/.`-yy- ..:/:/:hh    \n" +
            "o:oddhNNdhdhm+` -hs/ohs/+h:.. `.- . .:/osyy+osdNdmNmyhyoo-:.   :ms-`/:    -h/.dM/ :MN/`s:-:so+dm`   \n" +
            " /o.+ddoysdyody. `+hhhhsoo+ :` -:-  `.-::+y:ossdhddyyoyyhso://oshoo+/o.   .h/-+o/ :ho- .--+oo:md`   \n" +
            "-hmh.`/hssossyyh+  ysyddhyN/o    `:-//:/::d+hosNmmssysss++hy.../ssNs/++//::do+:/. .`.:- ..////hm`   \n" +
            "Nshyh` `odsoosoohh.+o+//::Msy+  :.```:::-+syoodNmdsshoso+so+s: //mN++dhhyyoosso:`        `/-yydd.   \n" +
            "dyysd-  .dmdNNmmdyhh/-+:.:Nydoo:`+` `+-++-ss/ydhmdhhhhhhyhhssyh:/yms+hshsy++o++/+/::::::/`.+hsdd-   \n" +
            "+::/:o+:myNhmydmh+ys/-/::-hNyds./..--o++oymh/hddmho/+o:s --+.+hmyos:+Nhdmhsoooo+/-:o/`  `-` -ddN-   \n" +
            ":-+o:---m/++//o+dhy+:+-+/:yMysy.hmMoMM+/-+so:yhdhhss://o`+.. -sss/s/sdhhyoso+o+ohhhso/h.`.`:oyyN-   \n" +
            ":-hMN/--dhmooys:+Mdo-s/:/-hddsd:yoMsym-+s+doohhhddo+/MN:+s-.-:yss/sooyoyys:--o:`     hd`-:+::o/N:   \n" +
            "--+-:/:`mMMshMN++Mmo:shoo.hhy+o::.: ..`//+NhydmNsm+++NM+mN.://sN+dhssssmshoso-s+     dd  -.:+:oMs   \n" +
            "syo+++o+NyNossyo/Nd+/-o--`mmdso::.:`:` //oymyhdNymy/y+/++` ://sm/sd/s+ymsoyMd/Md/    hN/ -.:dy+N+   \n" +
            "--.-.-.-syddmmdd/dh+-/sh:`hyms+o-/:/-s:s+ososddhhyy/oyh//: .:-oysyd+./yyhsh-/s//+    ym+ o`.syoms   \n" +
            ":/::-/--oyyo+y:yNyoo::+o``hydsy::----``o++ooshymhss/:oo+//  -/ooosy+:+hsydsoo/o//--  yds  `-sdhhd   \n" +
            "+/   - `yyhhd++:msoo-:+` `dyhs/.:`::: ..o-hsysyhhdh/:ds::/  :+ssoyy:./hyyds++-::/..  yms` `:osyyh   \n" +
            ".       yhyhy+o/moo+::/  `hssy:.o///`  -s++/sshhsmdo/os//o  .sshssy/`:mMhMhdNyhm-hy` +mo-` :+/hyy`  \n";

    public static final String forest = "                                                              -+                                    \n" +
            "                                                          `  `s:                                    \n" +
            "                                                            `ooh.                                   \n" +
            "              .+                                           `mh:ss.                                  \n" +
            "              +s                                           -N/`/h                                   \n" +
            "             /mN-                                        `/yN- /do`           `  .                  \n" +
            "             oNos                                        .-so/`+N+`           `  m.                 \n" +
            "           -s:o++                                        :s-hN/`mN:             +M-                 \n" +
            "          :s/s`dm+                                     /osyo/o./o+y`           `dMs                 \n" +
            "          /dN+`s+::                                    +/:sho`:.+syms         -/mNd`                \n" +
            "      `  .dyy+..ms-:                                   `sd+-+.`//+ym/        ``+Nd//                \n" +
            "      .  `-.mm:.hN+-`                                  .+o/oo+--:/dh+-        .hN-`o.               \n" +
            "         -/hNNo `sy+-                                `sy:::-d/o.o+/yd+. `  `.oyMM-.-y-              \n" +
            "        -+/-+NM+ -d/o`                               -y+:sh/y-ssmMyodd-    -s+yo/..yods.            \n" +
            "     . `+:+-+Nd::`dh+-`             /               /smyhds---.`+doo//:   ``:dsdd.`-dh:.            \n" +
            "        -ss+sN/`+ /Nh/`            `d               `+ysh:: ///  /mhsy ` `.-/dNmy/ -soys.           \n" +
            "        .so.-m-``s/o:++-          `sN:`            +yys:o.`--y-:. :oMh```-./oNMy:+ .+mym/.          \n" +
            "    `  .sd/`sN`  .+oho--:-`       :soyo-         `.:.:s::h/o:s-.o.:od/o+/ -o+hMo`.:::d+:+           \n" +
            "    ` +s+d+oh+-``-::myoo`.` .    :hN.m/ `        ```-NM:o/s:`:-/-s.yds:/- :oyNd`.`:/d+d:``          \n" +
            "      .+h+h+-`o`/ . /hd+    .` .`oMm`hh+``        .+mMmoo:.-`.:./:+/oNm+`-:sdm.`o`s`:yoN/           \n" +
            "       -+s/o.:+.-   ://dh--   .:+msy`..hy/`    ```oMMm:::h /-/``--/N/sh/.+NMMy+s+-h/-h/do.          \n" +
            "      `y+omh-o`.:`  /ysdN/` .//oNM//` `oydo   -dh+sNo:`:d:.-y-/` -:/h-h/sMddyNMdd`:-.-y/y:`         \n" +
            "     `y/:MMs:` .o`-/::mNM+/:...yMy+--` yNy-   `::sd:://ho`/+/h:+. ./+o/NdMd+`d+.s `/../+hy+`        \n" +
            "      :/hhN/--`sh`.om+-omNs:`.shNys /- -Ny:+`  :dy:`+o/-+-hh`ho/hh:-/ymMMs:..: .+ /..-::hs/-        \n" +
            "     -s/omNy/-hd/-`/:+/`oNMNdsNmdd:./.:`dN+:`.smMdo:No./.o-..yh`/sdoomMm+/h-   // /--/+`oo:-`       \n" +
            "     --hdmN/`hMo/s/+/-.` :dNMMMMNms/.-s-`y-```+sMh-ms`.`/- -.d-`-//yNMd/s//`   s/ +-ooos/oN-        \n" +
            "     .dN/+:`-dd.hMms/:--..`.//shdd+s`-s/--hs.`yNMNho--.`.`` .y:..-/++dMy+-- .``sy.-h-/yoNMm/.       \n" +
            "    `hMd.```:o:-mmMMmy+:--+:hdhsoNss`-/+::hNy`ohmMNh/:``o   -Mm-``:://NMMh- /.-+M+oh:`/h/dh-.`      \n" +
            "    .dN+`.`./o +s-smMMhho//shMMMMmsM`:s:y-.+m//oyMm:```+- .-oMM:+.-::+dMMh:sds:+MmMd-: ./ymo:.      \n" +
            "   /dmMo-``dy` hy/+osyhy++/++mMMMd:N`:m`yh-yMm/MMy.`::o-++y/sMM+m+y:-:/ohNMMNMMNdMNMy`:  .yNMm.     \n" +
            "   y++d:/--mo /+dd+/+ds+s/:-./ohddmN:+s.-md-yNdm+.-///:hyMM+dNhy/-/--`:+/hNN+mMM++soo +/`.omM+s-    \n" +
            "   /ho.:Ny-y`:/-o-/-md:myo--o/:::ymmmd//o/Nd:hN+:-:+--s/hyNs:h.:hs.-/:.-///ossNm+.o//: o+.+NMs-o/   \n" +
            "`-shss:sM+:-.y-+-`--h..s:doo////:osmMds/N.sMMMmo/-:`:o/h/dh.:y.-/sd/soh/-/ymm+m:y.`s-+. o+`.hMm-    \n" +
            ".yNymyhdm.`-yo./'``-- `/.-+y:ydo+dmsMs.+m/:dMd/.-/:++sh/dy.:`:o`:o/dMMMMmo:yN:Ny/y`.h.+` /s``yMm:   \n" +
            " sy-oMh-d:.m::o`-. `-   s:.-o.mMNhoMMN.hNds/MMNNMMMdmy/dmy+.yy-.`/yhNNMMMMh+NoNN/mo`hs:o.:dh-:+mN+ `\n" +
            ".osodMNNh-ho-+s./l.ydy/`ohh/do+NMMMMMNsMMNNdyN+oMMMM+omNNNN-hMh.-y-yoommoo::+hhMMMNhMNdMmdmshM+/h`+s\n" +
            "./-oMMMMNNd+dm:osooydMMhmMMmNNNNNMMMMMMMMo:m+. `y+mo-oys-:NhsMdoNhdo/shy+-`.y:+NMhmMh+dN:sN+-h: o-  \n" +
            " /dmhddhyyyy-hMNdo:hdmosNs:`/h+/+dMMhs:/o/ ..   `   ```` oyhsmy +`-. +o`.-``o `-///Mo`:h- .o-`.     \n" +
            "          `  sMs   `   `-   ./   -MN..  .``           `  `   yd````.           ``  dy  +o   :       \n" +
            "             sMs        `         yN`   `                    yM`                  .NN               \n" +
            "             yMd                  :M:                      ` hd`                 ` dM.              \n" +
            "             hMm `              - .N+                      .`my                  - yM+              \n" +
            "           : hMN-:              + .Nd                      -.Md` `                 +My       \n";
}
