package de.hsrw.powerteam.util;

public class TextUtils {
    public static String colorString(String color, String input) {
        return color + input + AsciiColors.ANSI_RESET;
    }

    public static String formattedDecision(int i, String description){
        return String.format(
                "%5s %s",
                AsciiColors.ANSI_CYAN + "[" + i + "]",
                AsciiColors.ANSI_RESET + description
        );
    }

    public static String formattedDecision(String command, String description) {
        return String.format(
                "%16s %s",
                AsciiColors.ANSI_CYAN + "[" + command + "]",
                AsciiColors.ANSI_RESET + description
        );
    }

    public static String formattedStat(String stat, int value) {
        return String.format(
                "%25s %s",
                AsciiColors.ANSI_PURPLE + stat,
                AsciiColors.ANSI_RESET + value
        );
    }

    public static String formattedStat(String stat, String value) {
        return String.format(
                "%25s %s",
                AsciiColors.ANSI_PURPLE + stat,
                AsciiColors.ANSI_RESET + value
        );
    }

    public static String formattedItem(int i, String name){
        return String.format(
                "%5s %30s",
                AsciiColors.ANSI_CYAN + "[" + i + "]",
                AsciiColors.ANSI_YELLOW + name + AsciiColors.ANSI_RESET
        );
    }

    public static String formattedShopItem(int i, String name, int cost){
        return String.format(
                "%5s %26s" + AsciiColors.ANSI_RESET + ": %s " + AsciiColors.ANSI_RESET + "monies",
                AsciiColors.ANSI_CYAN + "[" + i + "]",
                AsciiColors.ANSI_YELLOW + name,
                AsciiColors.ANSI_GREEN + cost
        );
    }

}
