package de.hsrw.powerteam;

import de.hsrw.powerteam.client.GameClient;
import de.hsrw.powerteam.server.WorldCreator;
import de.hsrw.powerteam.common.WorldData;
import de.hsrw.powerteam.server.ClientHandler;
import de.hsrw.powerteam.server.XmlStorage;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

public class Main {
    private static final int SERVER_PORT = 8888;
    public static ArrayList<ClientHandler> clientHandlers = new ArrayList<>();
    public static WorldData world;
    private static ServerSocket serverSocket;

    public static void main(String[] args) throws IOException {
        if (args[0].equals("server")) {
            serverSocket = new ServerSocket(SERVER_PORT);

            System.out.println("Server starting...");


            /*if (XmlStorage.checkWorldExists()) {
                System.out.println("loading world");
                XmlStorage.loadWorld();
            } else {*/
            System.out.println("generating and saving world...");
            world = WorldCreator.createWorld();
            XmlStorage.saveWorld(world);
            //}

            System.out.println("=== server started ===");

            while (true) {
                ClientHandler clientHandler = new ClientHandler(serverSocket.accept());
                new Thread(clientHandler).start();
                clientHandlers.add(clientHandler);
            }
        }

        if (args[0].equals("client")) {
            GameClient gameClient = new GameClient();
        }
    }

    protected void finalize() throws IOException {
        serverSocket.close();
    }
}
